#Glossary
# cost = x #==> cost in $ to build (subject to other modifiers)
# time = x #==> number of months to build.
# modifier = m # a modifier on the province that the building gives
# trigger = t # an and trigger that needs to be fullfilled to build and keep the building
# one_per_country = yes/no # if yes, only one of these can exist in a country
# manufactory = { trade_good trade_good } # list of trade goods that get a production bonus
# onmap = yes/no # show as a sprite on the map

################################################
# North American Natives
################################################

#Native Ceremonial Fire Pit
#Native Earthwork
#Native Fortified House
#Native Great Trail
#Native Irrigation
#Native Longhouse
#Native Palisade
#Native Storehouse
#Native Sweat Lodge
#Native Three Sisters Field

################################################
# Tier 1, 15th Century Buildings
################################################
native_ceremonial_fire_pit = {
	cost = 100
	time = 12
	trigger = {
		owner = { government = native_council }
	}
	modifier = {
		advisor_cost = -0.50
	}
	one_per_country = yes
	destroy_on_conquest = yes
	government_specific = yes
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
	}
}

native_earthwork = {
	cost = 100
	time = 12
	trigger = {
		owner = { government = native_council }
	}
	modifier = {
		local_defensiveness = 0.25
	}
	destroy_on_conquest = yes
	government_specific = yes
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 2.0
			is_capital = yes
		}
	}
}

native_fortified_house = {
	cost = 100
	time = 12
	trigger = {
		owner = { government = native_council }
	}
	modifier = {
		land_forcelimit = 10
	}
	one_per_country = yes
	destroy_on_conquest = yes
	government_specific = yes
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 2.0
			is_capital = yes
		}
	}
}

native_great_trail = {
	cost = 100
	time = 12
	trigger = {
		owner = { government = native_council }
	}
	modifier = {
		local_manpower_modifier = 0.5
	}
	destroy_on_conquest = yes
	government_specific = yes
	
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 2.0
			is_capital = yes
		}
	}
}

native_irrigation = {
	cost = 100
	time = 12
	trigger = {
		owner = { government = native_council }
	}
	modifier = {
		local_tax_modifier = 0.5
	}
	destroy_on_conquest = yes
	government_specific = yes
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
	}
}

native_longhouse = {
	cost = 100
	time = 12
	trigger = {
		owner = { government = native_council }
	}
	modifier = {
		tax_income = 1
	}
	one_per_country = yes
	destroy_on_conquest = yes
	government_specific = yes
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
	}
}

native_palisade = {
	cost = 100
	time = 12
	trigger = {
		owner = { government = native_council }
	}
	modifier = {
		fort_level = 1
	}
	destroy_on_conquest = yes
	government_specific = yes
	influencing_fort = yes
	
	onmap = yes
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 2.0
			is_capital = yes
		}
	}
}

native_storehouse = {
	cost = 100
	time = 12
	trigger = {
		owner = { government = native_council }
	}
	modifier = {
		local_production_efficiency = 0.5
	}
	destroy_on_conquest = yes
	government_specific = yes
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
	}
}

native_sweat_lodge = {
	cost = 100
	time = 12
	trigger = {
		owner = { government = native_council }
	}
	modifier = {
		diplomatic_reputation = 1
	}
	one_per_country = yes
	destroy_on_conquest = yes
	government_specific = yes
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
	}
}

native_three_sisters_field = {
	cost = 100
	time = 12
	trigger = {
		owner = { government = native_council }
	}
	modifier = {
		trade_goods_size_modifier = 0.5
	}
	destroy_on_conquest = yes
	government_specific = yes
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
	}
}
