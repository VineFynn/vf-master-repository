# Chinese civil war
cb_chinese_civil_war = {
	war_goal = unify_china
	
	ai_peace_desire = -20
	is_triggered_only = yes # See cb_types.1
	#valid_for_subject = no
	
	attacker_disabled_po = {
		po_become_tributary_state
	}
}

cb_chinese_civil_war_gp = {
	war_goal = unify_china_gp
	
	ai_peace_desire = 0
	is_triggered_only = yes
	
	months = 12000
	
	prerequisites = {
		OR = {
			AND = {
				culture_group = chinese_group
				NOT = { primary_culture = chinese_colonial }
			}
			has_country_flag = barbarian_claimant_china
		}
		is_neighbor_of = FROM
		FROM = {
			capital_scope = {
				OR = {
					superregion = north_china_superregion
					superregion = east_china_superregion
					superregion = southwest_china_superregion
				}
			}
			OR = {
				north_china_superregion = {
					owned_by = PREV
				}
				east_china_superregion = {
					owned_by = PREV
				}
				southwest_china_superregion = {
					owned_by = PREV
				}
			}
		}
		NOT = { overlord_of = FROM }
	}
	
	attacker_disabled_po = {
		po_become_tributary_state
	}
}
