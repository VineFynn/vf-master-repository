# Musellem Cavalry

unit_type = turkishtech
type = cavalry
maneuver = 2

#Tech 4
offensive_morale = 2
defensive_morale = 2
offensive_fire = 0
defensive_fire = 0
offensive_shock = 2
defensive_shock = 2

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}