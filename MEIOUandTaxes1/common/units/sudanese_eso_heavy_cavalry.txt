#24 - Slave Cavalry

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 4
defensive_morale = 5
offensive_fire = 0
defensive_fire = 1
offensive_shock = 5
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
