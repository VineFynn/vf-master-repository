# Zanbourak Camelry

unit_type = muslim
type = cavalry
maneuver = 2

#Tech 32
offensive_morale = 6
defensive_morale = 3
offensive_fire = 5
defensive_fire = 3
offensive_shock = 3
defensive_shock = 2

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}