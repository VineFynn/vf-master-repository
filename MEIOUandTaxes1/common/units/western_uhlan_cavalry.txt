#45 - Uhlans

type = cavalry
unit_type = western
maneuver = 2

offensive_morale = 7
defensive_morale = 4
offensive_fire = 4
defensive_fire = 3
offensive_shock = 7
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
