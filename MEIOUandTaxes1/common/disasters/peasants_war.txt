peasant_war = {
	potential = {
		government = monarchy
		NOT = { government = celestial_empire }
		NOT = { government = tribal_nomads_altaic }
		NOT = { government = tribal_nomads_steppe }
		NOT = { government = tribal_nomads }
		NOT = { government = tribal_nomads_hereditary }
		uses_doom = no
		NOT = { check_variable = { which = country_percent_urbanized value = 0.60 } }
		NOT = { government = maratha_confederacy }
		NOT = { government = rajput_monarchy }
		NOT = { government = indian_monarchy }
		NOT = {
			capital_scope = {
				continent = indian_continent
			}
		}
	}
	
	can_start = {
		NOT = { has_country_modifier = recent_peasants_war }
		NOT = { manpower_percentage = 0.30 }
	}
	
	can_stop = {
		manpower_percentage = 0.50
	}
	
	progress = {
		modifier = {
			factor = -3.0
			is_in_war = {
				defender_leader = ROOT
			}
		}
		modifier = {
			factor = 0.5
			war_exhaustion = 5
			hidden_trigger = { NOT = { war_exhaustion = 10 } }
		}
		modifier = {
			factor = 1
			war_exhaustion = 10
			hidden_trigger = { NOT = { war_exhaustion = 15 } }
		}
		modifier = {
			factor = 1.5
			war_exhaustion = 15
			hidden_trigger = { NOT = { war_exhaustion = 20 } }
		}
		modifier = {
			factor = 2
			war_exhaustion = 20
		}
		modifier = {
			factor = 0.5
			NOT = { stability = 0 }
			hidden_trigger = { stability = -1 }
		}
		modifier = {
			factor = 1
			NOT = { stability = -1 }
			hidden_trigger = { stability = -2 }
		}
		modifier = {
			factor = 1.5
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 0.5
			NOT = { legitimacy = 50 }
			hidden_trigger = { legitimacy = 40 }
		}
		modifier = {
			factor = 1
			NOT = { legitimacy = 40 }
			hidden_trigger = { legitimacy = 30 }
		}
		modifier = {
			factor = 1.5
			NOT = { legitimacy = 30 }
			hidden_trigger = { legitimacy = 20 }
		}
		modifier = {
			factor = 2
			NOT = { legitimacy = 20 }
			hidden_trigger = { legitimacy = 10 }
		}
		modifier = {
			factor = 2.5
			NOT = { legitimacy = 10 }
		}
		modifier = {
			factor = 3
			is_bankrupt = yes
		}
	}
	
	can_end = {
		NOT = {
			any_owned_province = {
				has_province_modifier = peasants_organizing
				unrest = 1
			}
		}
		#TODO, trigger on support for antitax rebels
	}
	
	modifier = {
		global_unrest = 5
		stability_cost_modifier = 0.20
	}
	
	on_start = peasants_war.1
	on_end = peasants_war.6
	
	on_monthly = {
		events = {
			
		}
		random_events = {
			1000 = 0
			100 = peasants_war.2
			100 = peasants_war.3
			# 100 = peasants_war.4
			100 = peasants_war.5
		}
	}
}