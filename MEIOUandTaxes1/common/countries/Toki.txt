#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 225  123  52 }

historical_idea_groups = {
	logistic_ideas
	trade_ideas
	popular_religion_ideas
	naval_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	quality_ideas
}

#Japanese group
historical_units = {
	asian_light_foot_infantry
	asian_horse_archer_cavalry
	asian_bushi_cavalry
	asian_shashu_no_ashigaru_infantry
	asian_samurai_cavalry
	asian_samurai_infantry
	asian_yarigumi_infantry
	asian_late_samurai_cavalry
	asian_arquebusier_infantry
	asian_musketeer_infantry
	asian_horse_guard_cavalry
	asian_new_guard_infantry
	asian_volley_infantry
	asian_armeblanche_cavalry
	asian_bayonet_infantry
	asian_lighthussar_cavalry
	asian_drill_infantry
	asian_columnar_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Yorimasu" = 100
	"Yoritada" = 100
	"Mochimasu" = 100
	"Yoriyasu" = 100
	"Yasuyuki" = 100
	"Shigeyori" = 100
	"Masafusa" = 100
	"Mochikane" = 100
	"Motoyori" = 100
	"Sadayori" = 100
	"Haruyori" = 100
	"Yoriaki" = 100
	"Yoritake" = 50
	"Yorizumi" = 50
	"Yoritsugu" = 100
	"Yorimoto" = 100
	"Mochiyori" = 100
	"Yasumasa" = 100
	"Akinao" = 50
	"Yorikiyo" = 100
	
	"Ako" = -1
	"Asahi" = -1
	"Aya" = -1
	"Harukiri" = -1
	"Inuwaka" = -1
	"Itoito" = -1
	"Itsuitsu" = -1
	"Koneneme" = -1
	"Mitsu" = -1
	"Narime" = -1
	"Sakami" = -1
	"Shiro" = -1
	"Tatsuko" = -1
	"Tomiko" = -1
	"Toyome" = -1
	"Yamabukime" = -1
}

leader_names = {
	# Daimyo names
	Shimazu Otomo Ryuzoji Aso Ito Shoni Ouchi Kono Chosokabe Miyoshi
	Mori Amago Yamana Akamatsu Hatakeyama Hosokawa Shiba Asakura Oda Tokugawa
	Togashi Uesugi Takeda Imagawa Tokugawa Hojo Nagao Satomi Satake Ustunomiya
	Date Mogami Ando Rokkaku Uragami Nanbu Ashina Murakami Saionji Akizuki Kimotuki
	Kasai Soma Yuki Saito Ogasawara Azai Hatano Jinbo Sagara Osaki
	Kitabatake Mimura Ichijo Toki Kyogoku Sasaki Ashikaga Isshiki Honganji
	# Toki Flavor
	Toki Toki Toki Toki Toki Toki Toki Toki Toki Toki
	Toki Toki Toki Toki Toki Toki Toki Toki Toki Toki
	# Retainers of the Toki clan
	Saito Saito Saito Saito Saito Akechi Akechi Akechi Akechi Akechi
	Hachiya Hachiya Hachiya Hachiya Hachiya Hara Hara Hara Hara Hara
	Asano Asano Asano Asano Asano Kanamori Kanamori Kanamori Kanamori Kanamori
}

ship_names = {
	"Mutsu Maru" "Dewa Maru" "Shimotsuke Maru"
	"Hitachi Maru"
	"Kazusa Maru" "Shimousa Maru" "Awa Maru" "Musashi Maru" "Kozuke Maru" "Izu Maru" "Echigo Maru" "Ettchu Maru"
	"Kaga Maru" "Noto Maru" "Suruga Maru" "Totomi Maru" "Joshu Maru" "Enshu Maru" "Bushu Maru" "Esshu Maru"
	"Mikawa Maru" "Ushu Maru" "Soushu Maru" "Boushu Maru" "Kai Maru" "Shinshu Maru" "Koshu Maru"
	"Owari Maru" "Bishu Maru" "Mino Maru" "Echizen Maru" "Ise Maru" "Shima Maru" "Omi Maru" 
	"Goushu Maru" "Yamashiro Maru" "Wakasa Maru" "Tanba Maru" "Tango Maru" "Yamato Maru" "Kii Maru"
	"Kishu Maru" "Iga Maru" "Sanuki Maru" "Tosa Maru"
	"Iyo Maru" "Harima Maru" "Banshu Maru"
	"Hoki Maru" "Inaba Maru" "Bizen Maru" "Mimasaka Maru" "Binshu Maru"
	"Bingo Maru" "Bittchu Maru" "Aki Maru" "Izumo Maru" "Iwami Maru" "Suou Maru" "Nagato Maru" "Bungo Maru"
	"Buzen Maru" "Chikuzen Maru"
	"Chikugo Maru" "Hizen Maru" "Higo Maru" "Chikushu Maru" "Hyuga Maru" "Osumi Maru" "Satsuma Maru" "Sasshu Maru"
}

army_names = {
	"Saito Gun" "$PROVINCE$ Gun" "$PROVINCE$ Zei"  "$PROVINCE$ Dan" 
}

fleet_names = {
	"Saito Suigun" "$PROVINCE$ Suigun" "$PROVINCE$ Sendan" "$PROVINCE$ Tai" 
}