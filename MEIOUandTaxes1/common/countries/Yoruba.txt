#Country Name: Yoruba
#Tag: YRB

graphical_culture = africangfx

color = { 181  184  179 }

historical_idea_groups = {
	trade_ideas
	leadership_ideas
	administrative_ideas
	quality_ideas
	aristocracy_ideas
	spy_ideas
	diplomatic_ideas
	logistic_ideas
}

historical_units = { #Coastal states
	sudanese_tuareg_camelry
	sudanese_archer_infantry
	sudanese_tribal_raider_light_cavalry
	sudanese_sword_infantry
	sudanese_war_raider_light_cavalry
	sudanese_sofa_infantry
	sudanese_pony_archer_cavalry
	sudanese_sofa_musketeer_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_gold_coast_infantry
	sudanese_musketeer_infantry
	sudanese_carabiner_cavalry
	sudanese_countermarch_musketeer_infantry
	unique_DAH_amazon_infantry
	sudanese_rifled_infantry
	sudanese_hunter_cavalry
	sudanese_impulse_infantry
	sudanese_lancer_cavalry
	sudanese_breech_infantry
}


monarch_names = {
	"Oranyan #0" = 20
	"Sango #0" = 20
	"Ajaka #0" = 20
	"Aganju #0" = 20
	"Kari #0" = 20
	"Oluaso #0" = 20
	"Onigbogi #0" = 20
	"Ofinran #0" = 20
	"Eguguoju #0" = 20
	"Orompoto #0" = 20
	"Ajiboyede #0" = 20
	"Abipa #0" = 20
	"Obalokun #0" = 20
	"Ajagbo #0" = 20
	"Odorawu #0" = 20
	"Kanran #0" = 20
	"Jayin #0" = 20
	"Ayibi #0" = 20
	"Osiyago #0" = 20
	"Ojigi #0" = 20
	"Gberu #0" = 20
	"Amuniwaiye #0" = 20
	"Onisle #0" = 20
	"Labisi #0" = 20
	"Agboluaje #0" = 20
	"Majeogbe #0" = 20
	"Abiodun #0" = 20
	"Awole Arongangan #0" = 20
	"Adebo #0" = 20
	"Maaku #0" = 20
	"Aburumaku #0" = 20
	"Majotu #0" = 20
	"Do Aklim #0" = 1
	"Dakondunu #0" = 1
	"Ganye Hessu #0" = 1
	"Wegbaja #0" = 1
	"Akaba #0" = 1
	"Agaja #0" = 1
	"Tegbesu #0" = 1
	"Kpengla #0" = 1
	"Anonglo #0" = 1
	"Haholo #0" = 1
	"Kpasse #0" = 1
	"Ayohuan #0" = 1
	"Agbangia #0" = 1
	
	"Hude #0" = -1
	"Amoako #0" = -1
	"Pokou #0" = -1
	"Emose #0" = -1
	"Amina #0" = -1
	"Lingeer #0" = -1
	"Orrorro #0" = -1
	"Saraounia #0" = -1
}

leader_names = {
	Ubah
	Umaru
	Aliu
	Solaja
	Okara
	Ajuwa
	Ezinwa
	Kanu
	Opara
	Rufai
}

ship_names = {
	Oranyan Ajaka Sango "Oyo-Ille" Oduduwa
	"Olorun Olodumare" "Ille-Ife" Odudua
	Obatala "O'odua" "Ile n'fe"
}
