country_decisions = {
	unite_nepal = {
		major = yes
		potential = {
			tag = NPL
			government = tribal_amalgamation
		}
		allow = {
			OR = {
				ai = no
				is_year = 1768
			}
		}
		effect = {
			change_government = indian_monarchy
			change_title_5 = yes
			define_ruler = {
				name = "Prithvi Narayana"
				dynasty = "Shah"
				DIP = 6
				ADM = 5
				MIL = 6
			}
			define_heir = {
				claim = 100
				name = "Pratapa Simha"
				dynasty = "Shah"
				DIP = 3
				ADM = 1
				MIL = 5
			}
			562 = {
				add_permanent_claim = NPL
			}
			523 = {
				add_permanent_claim = NPL
			}
			2701 = {
				add_permanent_claim = NPL
			}
			3113 = {
				add_permanent_claim = NPL
			}
			3112 = {
				add_permanent_claim = NPL
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	unite_bhutan = {
		major = yes
		potential = {
			tag = BHU
			government = tribal_amalgamation
		}
		allow = {
			OR = {
				ai = no
				is_year = 1616
			}
		}
		effect = {
			change_government = indian_monarchy
			change_title_4 = yes
			define_ruler = {
				name = "Ngawang Namgyal"
				dynasty = "Wangchuck"
				DIP = 6
				ADM = 6
				MIL = 5
			}
			define_heir = {
				claim = 100
				name = "Pekar Jungney"
				dynasty = "Wangchuck"
				DIP = 3
				ADM = 3
				MIL = 3
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	unite_sikkim = {
		major = yes
		potential = {
			tag = SKK
			government = tribal_amalgamation
		}
		allow = {
			OR = {
				ai = no
				is_year = 1642
			}
		}
		effect = {
			change_government = indian_monarchy
			change_title_2 = yes
			define_ruler = {
				name = "Phuntsog"
				dynasty = "Namgyal"
				DIP = 3
				ADM = 4
				MIL = 3
			}
			define_heir = {
				claim = 100
				name = "Tensung"
				dynasty = "Namgyal"
				DIP = 2
				ADM = 3
				MIL = 1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	unite_derbent = {
		major = yes
		potential = {
			tag = GAZ
			government = tribal_amalgamation
		}
		allow = {
			OR = {
				ai = no
				is_year = 1550
			}
		}
		effect = {
			change_government = tribal_confederation
			change_title_2 = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	unite_circasia = {
		major = yes
		potential = {
			tag = CIR
			government = tribal_amalgamation
		}
		allow = {
			OR = {
				ai = no
				is_year = 1777
			}
		}
		effect = {
			change_government = tribal_confederation
			change_title_3 = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	
	unite_baluchistan = {
		major = yes
		potential = {
			tag = BAL
			government = tribal_amalgamation
		}
		allow = {
			OR = {
				ai = no
				is_year = 1666
			}
		}
		effect = {
			change_government = indian_monarchy
			change_title_4 = yes
			define_ruler = {
				name = "Ahmad"
				dynasty = "Mirwani"
				DIP = 5
				ADM = 3
				MIL = 5
			}
			define_heir = {
				claim = 100
				name = "Mehrab"
				dynasty = "Mirwani"
				DIP = 3
				ADM = 3
				MIL = 3
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	unite_turkomans = {
		major = yes
		potential = {
			tag = KHO
			government = tribal_amalgamation
		}
		allow = {
			OR = {
				ai = no
				is_year = 1511
			}
		}
		effect = {
			change_government = tribal_confederation
			change_title_3 = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	unite_manipur = {
		major = yes
		potential = {
			tag = MLB
			government = tribal_amalgamation
		}
		allow = {
			OR = {
				ai = no
				is_year = 1800
			}
		}
		effect = {
			change_government = tribal_monarchy
			change_title_2 = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	unite_tripura = {
		major = yes
		potential = {
			tag = TPR
			government = tribal_amalgamation
		}
		allow = {
			OR = {
				ai = no
				is_year = 1463
			}
		}
		effect = {
			change_government = tribal_monarchy
			change_title_2 = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	unite_under_the_wahabis = {
		major = yes
		potential = {
			OR = {
				tag = ANZ
				tag = SMM
				tag = BHA
				tag = BTA
				tag = BKI
				tag = BYA
				tag = BKA
			}
			government = tribal_amalgamation
		}
		allow = {
			OR = {
				ai = no
				is_religion_enabled = wahhabi
			}
		}
		effect = {
			change_government = tribal_monarchy
			change_title_2 = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
	no_war_amalgamation = {
		major = yes
		potential = {
			government = tribal_amalgamation
		}
		allow = {
			is_at_war = yes
		}
		effect = {
			every_country = {
				limit = {
					war_with = ROOT
				}
				white_peace = ROOT
			}
		}
		ai_will_do = {
			factor = 100
		}
	}
	
}