country_decisions = {
	claim_celestial_empire = {
		major = yes
		potential = {
			NOT = { culture_group = chinese_group }
			OR = {
				technology_group = steppestech
				technology_group = chinese
				culture_group = tungusic
			}
			NOT = { has_country_flag = barbarian_claimant_china }
			chinese_imperial_gov_trigger = no
			is_free_or_tributary_trigger = yes
		}
		allow = {
			legitimacy = 70
			num_of_cities = 20
			OR = {
				owns = 708	# Beijing
				owns = 2252	# Xi'an
				owns = 1337	# Chengdu
				owns = 2121 # Guangdong
			}
		}
		effect = {
			set_country_flag = barbarian_claimant_china
			country_event = { id = red_turban.13 }
			add_prestige = 10
			if = {
				limit = {
					#NOT = { culture_group = tibetic }
					NOT = { primary_culture = korean }
					NOT = { primary_culture = daur }
					NOT = { culture_group = altaic }
					#NOT = { primary_culture = vietnamese }
					NOT = { culture_group = zuqun }
					NOT = { culture_group = japanese }
					#NOT = { dynasty = "Timurid" }
				}
				country_event = { id = tianxia.35 }
			}
			#if = {	#Tu?
			#	limit = {
			#		primary_culture = tibetan
			#	}
			#	country_event = { id = tianxia.41 }
			#}
			#if = {	#Xia or Qin
			#	limit = {
			#		primary_culture = qiang
			#	}
			#	country_event = { id = tianxia.42 }
			#}
			if = {	#Shen
				limit = {
					primary_culture = korean
				}
				country_event = { id = tianxia.43 }
			}
			#if = {	#Jin
			#	limit = {
			#		primary_culture = jurchen
			#	}
			#	country_event = { id = tianxia.44 }
			#}
			#if = {	#Tang or Jin or Han
			#	limit = {
			#		primary_culture = onggud
			#	}
			#	country_event = { id = tianxia.45 }
			#}
			#if = {	#Yue
			#	limit = {
			#		OR = {
			#			primary_culture = vietnamese
			#			culture_group = zuqun
			#		}
			#	}
			#	country_event = { id = tianxia.46 }
			#}
			if = {	#Liao
				limit = {
					OR = {
						primary_culture = daur
						primary_culture = khitan
					}
				}
				country_event = { id = tianxia.47 }
			}
			if = {	#Wu
				limit = {
					culture_group = japanese
				}
				country_event = { id = tianxia.48 }
			}
			#if = {	#Hui
			#	limit = {
			#		dynasty = "Timurid"
			#	}
			#	country_event = { id = tianxia.49 }
			#}
			north_china_superregion = { limit = { owned_by = ROOT } add_core = ROOT }
			#north_china_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
			east_china_superregion = { limit = { owned_by = ROOT } add_core = ROOT }
			#east_china_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
			southwest_china_superregion = { limit = { owned_by = ROOT } add_core = ROOT }
			#southwest_china_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROOT }
			#Cores moved to WF
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	# Vassal kings assassinate their Emperors - Chen Youliang, Zhu Yuanzhang
	betray_chinese_lord = {
		major = yes
		potential = {
			NOT = { has_country_flag = attempted_coup }
			culture_group = chinese_group
			is_subject_of_type = daimyo_vassal
			overlord = {
				has_country_flag = red_turban_reb
			}
		}
		allow = {
			OR = {
				overlord = {
					NOT = {
						army_strength = {
							who = ROOT
							value = 0.5
						}
					}
				}
				development_of_overlord_fraction = 3.0
				ai = no
			}
			mil_power = 200
			dip_power = 200
		}
		effect = {
			set_country_flag = attempted_coup
			country_event = { id = tianxia.4 }
			add_mil_power = -200
			add_dip_power = -200
		}
		ai_will_do = {
			factor = 1
			
			modifier = {
				is_at_war = yes
				factor = 0
			}
		}
		ai_importance = 100
	}
	
	
	# After successful proclamation of the dynasty we can compile the history of the previous dynasty
	compile_dynastic_history = {
		potential = {
			NOT = { has_country_flag = compiling_dynastic_history }
			NOT = { has_country_flag = compiled_dynastic_history }
			has_country_flag = mandate_of_heaven_claimed
			any_country = {
				has_country_flag = former_dynasty_china
			}
		}
		allow = {
			OR = {
				ADM = 5
				statesman = 1
			}
			adm_power = 200
		}
		effect = {
			set_country_flag = compiling_dynastic_history
			hidden_effect = {
				random_country = {
					limit = {
						has_country_flag = former_dynasty_china
					}
					save_global_event_target_as = former_dynasty_of_china
				}
			}
			country_event = { id = tianxia.100 days = 15 random = 5 }
			add_adm_power = -200
		}
		ai_will_do = {
			factor = 1
		}
	}
}
