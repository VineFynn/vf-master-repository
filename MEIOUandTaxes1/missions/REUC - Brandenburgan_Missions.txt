# Brandenburgan Missions

reclaim_niederlausitz = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		NOT = {
			tag = BRA
			tag = PRU
		}
		is_free_or_tributary_trigger = yes
		mil = 4
		owns = 50 # Brandenburg
		NOT = { owns_or_vassal_of = 2620 } # Niederlausitz
	}
	abort = {
		OR = {
			is_lesser_in_union = yes
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		owns = 2620 # Niederlausitz
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			dip = 4
		}
	}
	immediate = {
		add_claim = 2620 # Niederlausitz
	}
	abort_effect = {
		remove_claim = 2620 # Niederlausitz
	}
	effect = {
		2620 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
			add_territorial_core_effect = yes
		}
	}
}

reclaim_oberlausitz = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		NOT = {
			tag = BRA
			tag = PRU
		}
		is_free_or_tributary_trigger = yes
		mil = 4
		owns = 2620 # Niederlausitz
		NOT = { owns_or_vassal_of = 60 } # Oberlausitz
	}
	abort = {
		OR = {
			is_lesser_in_union = yes
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		owns = 60 # Oberlausitz
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			dip = 4
		}
	}
	immediate = {
		add_claim = 60 # Oberlausitz
	}
	abort_effect = {
		remove_claim = 60 # Oberlausitz
	}
	effect = {
		add_prestige = 5
		60 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
			add_territorial_core_effect = yes
		}
	}
}

reclaim_walcz = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		NOT = {
			tag = BRA
			tag = PRU
		}
		is_free_or_tributary_trigger = yes
		mil = 4
		owns = 264 # Neumark
		NOT = { owns_or_subject_of = 2834 } # Walcz
	}
	abort = {
		OR = {
			is_lesser_in_union = yes
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		owns = 2834 # Walcz
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			dip = 4
		}
	}
	immediate = {
		add_claim = 2834 # Walcz
	}
	abort_effect = {
		remove_claim = 2834 # Walcz
	}
	effect = {
		add_prestige = 5
		2834 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
			add_territorial_core_effect = yes
		}
	}
}

conquer_magdeburg = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = BRA
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { owns_or_vassal_of = 1350 } # Magdeburg
		NOT = { has_country_modifier = military_victory }
	}
	abort = {
		OR = {
			is_lesser_in_union = yes
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		owns = 1350 # Magdeburg
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			owns = 66 # Altmark
		}
	}
	immediate = {
		add_claim = 1350 # Magdeburg
	}
	abort_effect = {
		remove_claim = 1350 # Magdeburg
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		1350 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
			add_territorial_core_effect = yes
		}
	}
}
