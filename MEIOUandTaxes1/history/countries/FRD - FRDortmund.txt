# FRD - Frei Reichsstadt of Dortmund

government = imperial_city government_rank = 1
mercantilism = 0.0
primary_culture = westphalian
religion = catholic
technology_group = western
capital = 2159
fixed_capital = 2159


1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	set_country_flag = title_1
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1335.1.1  = {
	define_ruler = {
		name = "City County"
		ADM = 3
		DIP = 3
		MIL = 3
		regency = yes
	}
}
