#PAD - Padoa

government = signoria_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = venetian
religion = catholic
technology_group = western
capital = 1348	# Padoa
historical_rival = VEN

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	set_country_flag = title_2
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 20
}

1337.1.1 = {
	monarch = {
		name = "Marsilio"
		dynasty = "da Carrara"
		ADM = 3
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "Ubertino"
		monarch_name = "Ubertino"
		dynasty = "da Carrara"
		birth_date = 1300.1.1
		death_date = 1345.3.21
		claim = 95
		ADM = 4
		DIP = 2
		MIL = 3
	}
}

1338.3.22 = {
	monarch = {
		name = "Ubertino"
		dynasty = "da Carrara"
		ADM = 4
		DIP = 2
		MIL = 3
	}
	heir = {
		name = "Marsilietto"
		monarch_name = "Marsilietto"
		dynasty = "da Carrara"
		birth_date = 1300.1.1
		death_date = 1345.5.21
		claim = 95
		ADM = 4
		DIP = 2
		MIL = 3
	}
}

1345.3.7 = {
	monarch = {
		name = "Marsilietto"
		dynasty = "da Carrara"
		ADM = 3
		DIP = 2
		MIL = 1
	}
	heir = {
		name = "Jacopino"
		monarch_name = "Jacopino II"
		dynasty = "da Carrara"
		birth_date = 1320.1.1
		death_date = 1350.1.1
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 1
	}
}

1345.5.21 = {
	monarch = {
		name = "Jacopino II"
		dynasty = "da Carrara"
		ADM = 3
		DIP = 2
		MIL = 1
	}
	heir = {
		name = "Francesco"
		monarch_name = "Francesco"
		dynasty = "da Carrara"
		birth_date = 1340.1.1
		death_date = 1388.6.29
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 1
	}
}

1350.1.1 = {
	monarch = {
		name = "Francesco"
		dynasty = "da Carrara"
		ADM = 3
		DIP = 2
		MIL = 1
	}
}

1360.1.1 = {
	heir = {
		name = "Francesco"
		monarch_name = "Francesco Novello"
		dynasty = "da Carrara"
		birth_date = 1360.1.1
		death_date = 1405.1.17
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 1
	}
}

1388.6.29 = {
	monarch = {
		name = "Francesco Novello"
		dynasty = "da Carrara"
		ADM = 3
		DIP = 2
		MIL = 1
	}
	heir = {
		name = "Francesco"
		monarch_name = "Francesco III"
		dynasty = "da Carrara"
		birth_date = 1380.1.1
		death_date = 1445.1.1
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 1
	}
}

1405.1.17 = {
	monarch = {
		name = "Francesco III"
		dynasty = "da Carrara"
		ADM = 3
		DIP = 2
		MIL = 1
	}
}
