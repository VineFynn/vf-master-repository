# UTN - Utsunomiya
# LS/GG - Japanese Civil War
# 2010-jan-20 - FB - HT3
# 2013-aug-07 - GG - EUIV changes

government = japanese_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = kanto
religion = mahayana
technology_group = chinese
capital = 2292 # Shimotsuke

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1326.1.1 = {
	monarch = {
		name = "Ujitsuna"
		dynasty = "Utsunomiya"
		ADM = 4
		DIP = 5
		MIL = 3
	}
}

1350.1.1 = {
	heir = {
		name = "Mototsuna"
		monarch_name = "Mototsuna"
		dynasty = "Utsunomiya"
		birth_date = 1350.1.1
		death_date = 1380.6.19
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 4
	}
}
