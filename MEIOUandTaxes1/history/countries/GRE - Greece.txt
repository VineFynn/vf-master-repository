# GRE - Greece

government = feudal_monarchy government_rank = 1 #KINGDOM
mercantilism = 0.0
technology_group = eastern
primary_culture = greek
religion = orthodox
capital = 146	# Athens

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	set_country_flag = title_2
	#set_variable = { which = "centralization_decentralization" value = 1 }
	add_absolutism = -100
	add_absolutism = 40
}

1821.3.1 = {
	capital = 1399
}
1829.1.1 = {
	government = constitutional_republic
	monarch = {
		name = "Ioannis Kapodistrias"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
1832.1.1 = {
	government = feudal_monarchy remove_country_modifier = title_2 clr_country_flag = title_2 add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	monarch = {
		name = "Otto von Wittelsbach"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}
1834.1.1 = {
	capital = 146
}
1863.1.1 = {
	monarch = {
		name = "Georg I von Oldenburg"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}
