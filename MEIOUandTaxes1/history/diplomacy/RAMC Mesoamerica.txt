alliance = {
	first = TEN
	second = TEP
	start_date = 1351.1.1
	end_date = 1427.1.1
}
# Tarascan vassals
vassal = {
	first = PUR
	second = HUE
	start_date = 1350.1.1
	end_date = 1710.1.1
}

vassal = {
	first = PUR
	second = ACA
	start_date = 1350.1.1
	end_date = 1710.1.1
}

vassal = {
	first = PUR
	second = XAL
	start_date = 1350.1.1
	end_date = 1710.1.1
}


# INCA

vassal = {
	first = CZC
	second = AYA
	start_date = 1350.1.1
	end_date = 1463.1.1
}