# 1107 - Taranaki

capital = "Taranaki"
trade_goods = unknown # naval_supplies
culture = polynesian
religion = polynesian_religion

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 5
native_hostileness = 9

450.1.1 = {
	set_province_flag = tribals_control_province
}
1500.1.1 = {
	culture = maori
}
1642.1.1 = {
	discovered_by = NED
} # Abel Tasman
