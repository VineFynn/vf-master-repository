# 2401 - Poznanskie

owner = POL
controller = POL
add_core = POL

capital = "Poznan"
trade_goods = wool #linen
culture = polish
religion = catholic

hre = no

base_tax = 11
base_production = 0
base_manpower = 1

is_city = yes
marketplace = yes
temple = yes
workshop = yes
local_fortification_1 = yes
urban_infrastructure_1 = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1355.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1490.1.1 = {
	unrest = 6
} # Uprising led by Mukha
1492.1.1 = {
	unrest = 0
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 15
	base_production = 2
	base_manpower = 1
	fort_14th = yes
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1588.1.1 = {
	controller = REB
} # Civil war
1589.1.1 = {
	controller = PLC
} # Coronation of Sigismund III
1596.1.1 = {
	unrest = 4
} # Religious struggles, union of Brest
1597.1.1 = {
	unrest = 0
}
1733.1.1 = {
	controller = REB
} # The war of Polish succession
1735.1.1 = {
	controller = PLC
}
1793.1.23 = {
	controller = PRU
	owner = PRU
	add_core = PRU
	add_core = POL
	remove_core = PLC
} # Second partition
1807.7.9 = {
	owner = POL
	controller = POL
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = {
	controller = PRU
}
1814.4.11 = {
	controller = POL
}
1815.6.9 = {
	owner = PRU
	controller = PRU
	add_core = PRU
}