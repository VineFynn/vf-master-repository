# 2966 - Kindia

owner = FLO
controller = FLO
add_core = FLO

capital = "Kindia"
trade_goods = livestock
culture = fulani
religion = sunni

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 96 }
}
1520.1.1 = {
	base_tax = 9
}