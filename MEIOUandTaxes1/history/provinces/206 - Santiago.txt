# 206 - Santiago

owner = CAS #Juan II of Castille
controller = CAS

capital = "Santiago de Compostela"
trade_goods = livestock
culture = galician
religion = catholic

hre = no

base_tax = 13
base_production = 1
base_manpower = 2

is_city = yes
marketplace = yes
town_hall = yes
temple = yes
local_fortification_1 = yes
road_network = yes

# Santiago de Compostela pilgrimage

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1 = {
	owner = ENR
	controller = ENR
	add_core = ENR
	add_core = CAS
	add_core = GAL
	add_permanent_province_modifier = {
		name = "kingdom_of_leon"
		duration = -1
	}
}
1369.3.23 = {
	remove_core = ENR
	owner = CAS
	controller = CAS
}
1400.1.1 = {
	fort_14th = yes
}
1467.1.1 = {
	unrest = 4
} # Second war of the "irmandiņos"
1470.1.1 = {
	unrest = 0
} # End of the Second war of the "irmandiņos"
1475.6.2 = {
	controller = POR
}
1476.3.2 = {
	controller = CAS
}
1500.3.3 = {
	base_tax = 15
	base_production = 1
	base_manpower = 2
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
	road_network = no
	paved_road_network = yes
	remove_core = GAL
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castilla
1713.4.11 = {
	remove_core = GAL
}
1808.6.6 = {
	controller = REB
}
1809.1.1 = {
	controller = SPA
}
1812.10.1 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
