# 3727 - Hildesheim

owner = HIL
controller = HIL
add_core = HIL

capital = "Hildesheim"
trade_goods = livestock
culture = eastphalian
religion = catholic

hre = yes

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
road_network = yes
local_fortification_1 = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 5
	base_production = 1
}
1542.1.1 = {
	religion = protestant
}
1813.10.14 = {
	owner = HAN
	controller = HAN
	add_core = HAN
}
