#126 - Malta

owner = SIC
controller = SIC
add_core = SIC

capital = "L-Imdina"
trade_goods = fish
culture = maltese
religion = catholic

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
harbour_infrastructure_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "malta_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_core = ARA
	add_core = KNP
}
1409.1.1 = {
	owner = ARA
	controller = ARA
}
1516.1.23 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = ARA
} # Unification of Spain
1520.5.5 = {
	base_tax = 1
	base_production = 2
	base_manpower = 0
	harbour_infrastructure_1 = no
	harbour_infrastructure_2 = yes
	military_harbour_1 = yes
	workshop = yes
	town_hall = yes
}
1530.1.1 = {
	controller = KNI
	owner = KNI
	add_core = KNI
	remove_core = SPA
	fort_14th = yes
} # Malta given to the Knights
1566.3.28 = {
	capital = "Il-Belt"
}
1571.1.1 = {
	fort_14th = no
	fort_16th = yes
} # Valletta completed
1775.1.1 = {
	unrest = 4
} # Revolt headed by priest Gaetano Mannarino failed.
1776.1.1 = {
	unrest = 0
}
1798.6.9 = {
	controller = FRA
} # Occupied by French troops
1800.1.1 = {
	controller = KNI
} # British protectorate
1814.5.30 = {
	owner = GBR
	controller = GBR
	add_core = GBR
} # Treaty of Paris
