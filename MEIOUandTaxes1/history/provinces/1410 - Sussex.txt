# 1410 - Sussex

owner = ENG
controller = ENG
add_core = ENG

capital = "Chichester"
trade_goods = livestock
culture = english
religion = catholic

hre = no

base_tax = 10
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
temple = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

1250.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = iron
	set_province_flag = freeholders_control_province
}
#1356.1.1 = {
#	add_core = WSS
#}
1453.1.1 = {
	unrest = 5
} # Start of the War of the Roses
1459.1.1 = {
	unrest = 7
} # Increasing Disorder & Piracy Along South Coast
1460.1.1 = {
	controller = REB
}
1461.6.1 = {
	unrest = 2
	controller = ENG
} # Coronation of Edward IV
1467.1.1 = {
	unrest = 5
} # Rivalry between Edward IV & Warwick
1471.1.1 = {
	unrest = 8
} # Unpopularity of Warwick & War with Burgundy
1471.5.4 = {
	unrest = 2
} # Murder of Henry VI & Restoration of Edward IV
1483.6.26 = {
	unrest = 8
} # Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = {
	unrest = 0
} # Battle of Bosworth Field & the End of the War of the Roses
1520.5.5 = {
	base_tax = 15
	base_production = 0
	base_manpower = 1
}
1525.1.1 = {
	fort_14th = yes
	weapons = yes
}
1538.1.1 = {
	religion = protestant #anglican
}
1580.1.1 = {
	religion = reformed
}
1642.8.22 = {
	controller = REB
} # Start of First English Civil War
1646.5.5 = {
	controller = ENG
} # End of First English Civil War
1648.5.11 = {
	controller = REB
}
1648.6.1 = {
	controller = ENG
}
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1750.1.1 = { } # Estimated
