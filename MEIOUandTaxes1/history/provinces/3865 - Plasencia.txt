# No previous file for Plasencia

owner = CAS #Juan II of Castille
controller = CAS
add_core = CAS
add_core = LEO

capital = "Plasencia"
trade_goods = wool
culture = asturian
religion = catholic

hre = no

base_tax = 3
base_production = 2
base_manpower = 0

is_city = yes
road_network = yes
workshop = yes
urban_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = ENR
	add_permanent_province_modifier = {
		name = "kingdom_of_leon"
		duration = -1
	}
}
1369.3.23 = {
	remove_core = ENR
}
1464.5.1 = {
	unrest = 3
} #Nobiliary uprising against King Enrique, Castilla goes into anarchy
1468.9.18 = {
	unrest = 0
} #Pactos de los Toros de Guisando. Isabel of Castille becomes heir to the throne and a temporary peace is achieved
1470.1.1 = {
	unrest = 3
} #Isabel marries with Fernando of Aragon, breaking the Pacts of Guisando. King Enrique choses his daughter Juana ("La Beltraneja") as new heiress and a succession War erupts.
1475.6.2 = {
	controller = POR
}
1476.3.2 = {
	controller = CAS
}
1479.9.4 = {
	unrest = 0
} #Peace of Alca�ovas, between Queen Isabel and King Alfonso of Portugal who had entered the war supporting her wife Juana
1500.3.3 = {
	base_tax = 4
	base_production = 2
	base_manpower = 0
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
	road_network = no
	paved_road_network = yes
	remove_core = LEO
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castilla
1520.5.1 = {
	unrest = 5
} # War of the Comunidades
1521.4.1 = {
	unrest = 0
} # The army of the "Comuneros" is defeated at Villalar. Its leaders are promptly beheaded.
1600.1.1 = {
	culture = castillian
} #culture = extremaduran
1713.4.11 = {
	remove_core = LEO
}
