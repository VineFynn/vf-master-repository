# 249 - Ayrshire

owner = SCO
controller = SCO
add_core = SCO

capital = "Ayr"
trade_goods = livestock
culture = highland_scottish
religion = catholic

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
temple = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = {
		name = "clyde_large_natural_harbour"
		duration = -1
	}
}
1451.1.1 = {
	small_university = yes
}
1520.5.5 = {
	base_tax = 3
	base_production = 1
	base_manpower = 0
}
1560.8.1 = {
	religion = reformed
}
1600.1.1 = {
	capital = "Glesca"
	fort_14th = yes
} #marketplace Estimated
1645.8.1 = {
	controller = REB
} #Estimated
1645.9.13 = {
	controller = SCO
} #Battle of Philiphaugh
1651.4.1 = {
	controller = ENG
}
1652.4.21 = {
	controller = SCO
} #Union of Scotland and the Commonwealth
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
