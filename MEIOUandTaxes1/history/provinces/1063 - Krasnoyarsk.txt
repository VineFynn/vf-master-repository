# 1063 - Angara

owner = YUA
controller = YUA
add_core = YUA

capital = "Angara"
trade_goods = fur
culture = buryat
religion = tengri_pagan_reformed

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	add_core = BRT
}
1392.1.1 = {
	owner = BRT
	controller = BRT
	remove_core = YUA
}
1632.1.1 = {
	discovered_by = RUS
}
1632.9.25 = {
	owner = RUS
	controller = RUS
#	religion = orthodox
#	culture = russian
	capital = "Bratsk"
}
1657.1.1 = {
	add_core = RUS
}
