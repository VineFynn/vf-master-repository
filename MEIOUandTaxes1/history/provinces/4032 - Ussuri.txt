# No previous file for Ussuri

owner = MRC
controller = MRC
add_core = MRC

capital = "Ulahe"
trade_goods = lumber #naval_supplies
culture = jurchen
religion = tengri_pagan_reformed

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese
discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1420.1.1 = {
	owner = MUD
	controller = MUD
	add_core = MUD
	remove_core = MRC
}
1530.1.1 = {
	marketplace = yes
}
1616.2.17 = {
	owner = JIN
	controller = JIN
	add_core = JIN
	remove_core = MRC
}
1635.1.1 = {
	culture = manchu
}
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = JIN
} # The Qing Dynasty
1858.1.1 = {
	owner = RUS
	controller = RUS
	religion = orthodox
	culture = russian
} # Treaty of Aigun
