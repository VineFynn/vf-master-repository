# 4009 - Aachen

owner = AAC
controller = AAC
add_core = AAC

capital = "Aachen"
trade_goods = wheat #cloth
culture = ripuarianfranconian
religion = catholic

hre = yes

base_tax = 5
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
town_hall = yes
corporation_guild = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	#add_permanent_province_modifier = {
	#	name = urban_goods_beer
	#	duration = -1
	#}
	set_province_flag = pocket_province
}
450.1.1 = {
	add_permanent_province_modifier = {
		name = throne_of_charlemagne
		duration = -1
	}
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 4
	base_production = 2
	base_manpower = 0
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1690.1.1 = {
	base_manpower = 1
}
1730.1.1 = {
	base_manpower = 2
} #
1792.12.15 = {
	controller = FRA
} # Occupied by French troops
1797.10.17 = {
	owner = FRA
	add_core = FRA
} # The Treaty of Campo Formio
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1814.4.6 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = FRA
} # Napoleon abdicates
