# 2910 - Mali

owner = MAL
controller = MAL
add_core = MAL

capital = "Niani"
trade_goods = rice
culture = maninka
religion = sunni

hre = no

base_tax = 6
base_production = 4
base_manpower = 1

is_city = yes
marketplace = yes
urban_infrastructure_2 = yes
workshop = yes
fort_14th = yes

is_city = yes

discovered_by = soudantech
discovered_by = sub_saharan

1528.1.1 = {
	base_tax = 5
}