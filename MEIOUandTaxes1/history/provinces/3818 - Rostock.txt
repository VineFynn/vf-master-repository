# 3818 - Rostock

owner = MKL
controller = MKL
add_core = MKL

capital = "Rostock"
trade_goods = lumber
culture = pommeranian
religion = catholic

hre = yes

base_tax = 3
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
harbour_infrastructure_2 = yes
marketplace = yes
town_hall = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "rostock_natural_harbour"
		duration = -1
	}
}
1419.1.1 = {
	small_university = yes
}
1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1530.1.1 = {
	religion = protestant
}
1573.9.21 = {
	owner = MKL
	controller = MKL
}
1628.1.1 = {
	controller = SWE
} # First Treaty
1648.10.24 = {
	controller = MKL
} # Treaty of Westphalia
1700.1.1 = {
	controller = DEN
}
1721.1.1 = {
	controller = MKL
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.1.1 = {
	owner = FRA
	controller = FRA
} # French occupation
1813.10.13 = {
	owner = SWE
	controller = SWE
}
