# No previous file for Mnisota

capital = "Mnisota"
trade_goods = unknown
culture = dakota
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 55
native_ferocity = 3
native_hostileness = 4

450.1.1 = {
	set_province_flag = tribals_control_province
}
1650.1.1 = {
	owner = SIO
	controller = SIO
	add_core = SIO
	trade_goods = fur
	is_city = yes
} # Position of the natives circal the Beaver Wars
