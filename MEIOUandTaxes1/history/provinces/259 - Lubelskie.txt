owner = POL
controller = POL
add_core = POL

capital = "Lublin"
trade_goods = wheat #beer
culture = polish
religion = catholic

hre = no

base_tax = 8
base_production = 0
base_manpower = 1

is_city = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 12
	base_production = 1
	base_manpower = 1
	fort_14th = yes
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1588.1.1 = {
	controller = REB
} # Civil war
1589.1.1 = {
	controller = PLC
} # Coronation of Sigismund III
1606.1.1 = {
	controller = REB
} # Civil war
1608.1.1 = {
	controller = PLC
} # Minor victory of Sigismund
1655.1.1 = {
	controller = SWE
} # The Deluge
1660.1.1 = {
	controller = PLC
}
1733.1.1 = {
	controller = REB
} # The war of Polish succession
1735.1.1 = {
	controller = PLC
}
1795.10.24 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = POL
	remove_core = PLC
} # Third partition
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1806.11.3 = {
	controller = REB
} # Polish uprising instigated by Napoleon
1809.10.14 = {
	owner = POL
	controller = POL
	remove_core = PRU
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = {
	controller = RUS
}
1814.4.11 = {
	controller = POL
}
1815.6.9 = {
	add_core = RUS
} # Congress Poland, under Russian control after the Congress of Vienna
