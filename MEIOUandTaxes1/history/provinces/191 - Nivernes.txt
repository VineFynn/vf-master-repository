# 191 Nevers - Principal cities: Nevers

owner = BUR
controller = BUR
add_core = BUR

capital = "Nevers"
trade_goods = iron #wheat
culture = bourguignon
religion = catholic

hre = no

base_tax = 11
base_production = 0
base_manpower = 1

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

862.1.1 = {
	owner = FLA
	controller = FLA
	add_core = FLA
}
1356.1.1 = {
	add_core = NEV
	add_core = FRA
	set_province_flag = add_local_autonomy_25
}
1384.1.1 = {
	owner = BUR
	controller = BUR
}
1400.1.1 = {
	temple = yes
}
1444.1.1 = {
	remove_core = FRA
}
1477.1.5 = {
	add_core = FRA
}
1477.1.5 = {
	owner = NEV
	controller = NEV
}
1491.1.1 = {
	add_core = KLE
	remove_core = BUR
}
1520.5.5 = {
	base_tax = 13
	base_manpower = 1
}
1530.1.1 = {
	fort_14th = yes
}
1530.1.2 = {
	owner = FRA
	controller = FRA
	add_core = FRA
}
1565.1.1 = {
	add_core = MAN
	remove_core = KLE
}
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = {
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
1639.1.1 = {
	unrest = 3
}
1640.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1641.1.1 = {
	unrest = 0
}
1650.1.14 = {
	unrest = 7
} # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1 = {
	unrest = 4
} # An unstable peace is concluded
1651.12.1 = {
	unrest = 7
} # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = {
	unrest = 0
} # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1659.1.1 = {
	owner = FRA
	controller = FRA
	remove_core = MAN
}
1710.1.1 = {
	fort_15th = no
	fort_16th = yes
}

