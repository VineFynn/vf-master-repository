# 3356 - Izumi

owner = HKW
controller = HKW
add_core = HKW

capital = "Izumi"
trade_goods = rice
culture = kansai
religion = mahayana

hre = no

base_tax = 7
base_production = 0
base_manpower = 0

is_city = yes
fort_14th = yes

discovered_by = chinese

1356.1.1 = {
	add_core = MIY
}
1359.1.1 = {
	controller = HKY
	owner = HKY
}
1360.1.1 = {
	controller = HKW
	owner = HKW
}
1362.1.1 = {
	controller = JAP
} # Conquered for the southern court by Kusunoki Masanori ADD TAG
1378.1.1 = {
	add_core = YMN
	controller = YMN
	owner = YMN
}
1391.1.1 = {
	add_core = OUC
	controller = OUC
	owner = OUC
}
1400.1.1 = {
	controller = NIK
	owner = NIK
	add_core = NIK
}
1408.1.1 = {
	owner = HKW
	controller = HKW
}
1501.1.1 = {
	base_tax = 11
	base_manpower = 1
}
1535.1.1 = {
	owner = MIY
	controller = MIY
}
1542.1.1 = {
	discovered_by = POR
}
1572.1.1 = {
	owner = ODA
	controller = ODA
}
1583.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
