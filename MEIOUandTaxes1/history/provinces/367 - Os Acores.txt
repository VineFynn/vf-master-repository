# 367 - The Azores

capital = "Ponta Delgada"
trade_goods = fish	#could be wheat
culture = portugese
religion = catholic

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 0
native_ferocity = 0
native_hostileness = 0

1427.1.1 = {
	discovered_by = POR
	owner = POR
	controller = POR
	citysize = 100
	is_city = yes
}
1452.1.1 = {
	add_core = POR
	citysize = 500
	naval_arsenal = yes
	marketplace = yes
	customs_house = yes
}
1500.1.1 = {
	is_city = yes
}
1500.3.3 = {
	base_tax = 2
	base_production = 0
	base_manpower = 0
}
