# 1062 - Verkohzehkoye

capital = "Verkohzehkoye"
trade_goods = fur
culture = oroqen
religion = tengri_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 1
native_hostileness = 3

discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1643.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
#	religion = orthodox
#	culture = russian
} # Founded by Pyotr Beketov
1668.1.1 = {
	add_core = RUS
}
