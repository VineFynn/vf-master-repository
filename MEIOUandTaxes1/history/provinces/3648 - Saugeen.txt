# No previous file for Saugeen

capital = "Saugeen"
trade_goods = unknown
culture = odawa
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 2
native_hostileness = 5

450.1.1 = {
	set_province_flag = tribals_control_province
}
1650.1.1 = {
	owner = OTW
	controller = OTW
	add_core = OTW
	is_city = yes
	trade_goods = fur
}
