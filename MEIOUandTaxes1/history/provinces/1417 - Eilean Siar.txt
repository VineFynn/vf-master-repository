# 1417 - Eileanan Siar

owner = NOR
controller = NOR

capital = "Ste�rnabhagh"
trade_goods = fish
culture = highland_scottish
religion = catholic

hre = no

base_tax = 0
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

1266.1.1 = {
	owner = TLI
	controller = TLI
	add_core = TLI
}
1493.1.1 = {
	owner = SCO
	controller = SCO
	add_core = SCO
}
1560.8.1 = {
	religion = reformed
}
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
1750.1.1 = {
	fort_14th = yes
}
