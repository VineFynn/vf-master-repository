# 102 - Nice

owner = NIC
controller = NIC
add_core = NIC

capital = "Nissa"
trade_goods = olive
culture = ligurian
religion = catholic

hre = yes

base_tax = 6
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
temple = yes
harbour_infrastructure_1 = yes
town_hall = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "nice_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_permanent_claim = PRO
}
1384.1.1 = {
	add_core = ANJ
	add_core = FRA
	remove_core = PRO
}
1388.9.28 = {
	owner = SAV
	controller = SAV
	add_core = SAV
	remove_core = ANJ
	remove_core = FRA
} # Dedition of Nice to the Duchy of Savoie
1520.5.5 = {
	base_tax = 8
	base_production = 1
	base_manpower = 0
}
1522.3.20 = {
	naval_arsenal = yes
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1530.2.27 = {
	hre = no
}
1550.1.1 = {
	fort_14th = yes
}
1618.1.1 = {
	hre = no
}
1626.1.1 = { } #Proclaiming full freedom of trade, Charles Emmanuel in 1626 gave a great stimulus to the commerce of the city
1705.12.30 = {
	fort_15th = no
	controller = FRA
} # Louis XIV occupied the city
1713.4.11 = {
	owner = SIC
	controller = SIC
	add_core = SIC
	remove_core = SAV
} # Treaty of Utrecht(2) gave Sicily to House of Savoy
1718.8.2 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = SIC
} # Kingdom of Piedmont-Sardinia
1744.1.1 = {
	controller = FRA
}
1748.10.18 = {
	controller = SPI
}
1792.9.21 = {
	controller = FRA
} # Conquered by the French
1796.4.25 = {
	owner = FRA
	add_core = FRA
} # The Republic of Alba
1814.4.11 = {
	owner = SPI
	controller = SPI
	remove_core = FRA
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1860.1.13 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = SPI
}
