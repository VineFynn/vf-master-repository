# 3363 - Bingo

owner = HKW
controller = HKW
add_core = HKW

capital = "Futu"
trade_goods = rice
culture = chugoku
religion = mahayana

hre = no

base_tax = 14
base_production = 0
base_manpower = 1

is_city = yes

discovered_by = chinese

1356.1.1 = {
	add_core = OUC
	add_core = SKW #home province
}
1365.1.1 = {
	owner = SKW
	controller = SKW
}
1371.1.1 = {
	add_core = IGW
	owner = IGW
	controller = IGW
}
1379.1.1 = {
	add_core = YMN
	owner = YMN
	controller = YMN
}
1390.1.1 = {
	owner = HKW
	controller = HKW
}
1401.1.1 = {
	owner = YMN
	controller = YMN
}
1501.1.1 = {
	base_tax = 25
	base_manpower = 2
}
1542.1.1 = {
	discovered_by = POR
}
1552.1.1 = {
	add_core = ANG
	owner = ANG
	controller = ANG
}
1562.1.1 = {
	owner = MRI
	controller = MRI
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
