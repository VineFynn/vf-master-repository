# No previous file for Hetu Ala

owner = YUA
controller = YUA

capital = "Sunggari Hala"
trade_goods = iron
culture = uriankhai
religion = tengri_pagan_reformed

hre = no

base_tax = 25
base_production = 0
base_manpower = 1

is_city = yes

discovered_by = chinese
discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1271.12.18 = {
	add_core = YUA
} # Proclamation of Yuan dynasty
1308.1.1 = {
	owner = CSE
	controller = CSE
	add_core = CSE
} # Shen viceroy
1345.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
	add_core = MXI
	remove_core = CSE
	fort_14th = yes
}
1376.1.1 = {
	owner = MXI
	controller = MXI
	remove_core = YUA
}
1411.5.1 = {
	owner = MJZ
	controller = MJZ
	add_core = MJZ
	remove_core = MXI
	culture = jurchen
} # Menggtemu settles Odoli tribe
1530.1.1 = {
	marketplace = yes
	temple = yes
	weapons = yes
}
1542.1.1 = {
	owner = MCH
	controller = MCH
	add_core = MCH
	remove_core = MJZ
} # Aisin Gioro enclipse Odoli
1616.2.17 = {
	owner = JIN
	controller = JIN
	add_core = JIN
	remove_core = MCH
}
1635.1.1 = {
	culture = manchu
}
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = JIN
} # The Qing Dynasty
1709.1.1 = {
	discovered_by = SPA
}
