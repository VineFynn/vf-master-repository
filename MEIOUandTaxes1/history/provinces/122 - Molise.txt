# 122 - Capitanata

owner = KNP
controller = KNP
add_core = KNP

capital = "Campuasce"
trade_goods = olive
culture = neapolitan
religion = catholic

hre = no

base_tax = 12
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
temple = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = "lack_of_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_core = ANJ
}
1442.1.1 = {
	add_core = ARA
}
1495.2.22 = {
	controller = FRA
} # Charles VIII invades Naples
1495.7.7 = {
	controller = KNP
} # Charles VIII leaves Italy
1502.1.1 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # France and Aragon partitions Naples
1503.6.1 = {
	owner = ARA
	controller = ARA
	add_core = ARA
	remove_core = FRA
	remove_core = ANJ
} # France forced to withdraw
1504.1.31 = {
	remove_core = FRA
} # Treaty of Lyon
1516.1.23 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = ARA
	road_network = no
	paved_road_network = yes
} # Unification of Spain
1520.5.5 = {
	base_tax = 14
	base_production = 0
	base_manpower = 1
}
1530.1.1 = {
	owner = KNP
	controller = KNP
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
}
1714.3.7 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = SPA
}
1734.6.2 = {
	owner = NAP
	controller = NAP
	add_core = NAP
	remove_core = HAB
}
#1815.5.3 = {
#	owner = KNP
#	controller = KNP
#	remove_core = NAP
#}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
