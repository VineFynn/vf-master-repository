# 2355 - Gd�nsk

owner = DNZ
controller = DNZ
add_core = DNZ

capital = "Danzig" # (German), Gdansk (Polish), Gdunsk (Kashubian)
trade_goods = fish #cloth
culture = prussian
religion = catholic

hre = no

base_tax = 1
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
harbour_infrastructure_2 = yes
workshop = yes
town_hall = yes
temple = yes
marketplace = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = {
		name = vistula_estuary_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = "danzig_large_natural_harbour"
		duration = -1
	}
}
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_cloth
		duration = -1
	}
}
1356.1.1 = {
	add_core = TEU
	set_province_flag = add_local_autonomy_50 # Still recovering after the Danzig slaughter
}
1444.1.1 = {
	culture = prussian
	military_harbour_1 = yes # Hanseatic shipyard.
}
1466.10.19 = {
	add_core = POL
	remove_core = TEU
	set_province_flag = add_local_autonomy_50 # Granted significant privileges
} # Peace treaty, "Peace of Torun"
1496.1.1 = {
	temple = yes
}
1520.5.5 = {
	base_tax = 1
	base_production = 3
	base_manpower = 0
}
1522.3.20 = {
	naval_arsenal = yes
}
1524.1.25 = {
	unrest = 6
} # Debt crisis
1525.1.1 = {
	unrest = 0
	religion = protestant
}
#1525.4.10 = {
#	add_core = PRU
#}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1569.7.1 = {
	add_core = PLC
	remove_core = POL
} # Union of Lublin
1572.1.1 = {
	unrest = 6
} # Sigismund II dies
1576.1.1 = {
	unrest = 8
} # Danzig rebellion
1577.6.13 = {
	controller = PLC
} # Danzig War, under siege by Poland
1588.1.1 = {
	controller = REB
} # Civil war
1589.1.1 = {
	controller = DNZ
	unrest = 0
} # Coronation of Sigismund III
1600.1.1 = {
	fort_14th = yes
}
1704.1.1 = {
	controller = SWE
} # Under siege by Sweden
1709.1.1 = {
	controller = DNZ
} # The Swedes lost the battle of Poltava
1793.1.23 = {
	owner = PRU
	controller = PRU
	add_core = PRU
} # Occupied and later on annexed by Prussia
1794.3.24 = {
	unrest = 5
} # Kosciuszko uprising
1794.11.16 = {
	unrest = 0
} # The end of the uprising
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.3.1 = {
	controller = FRA
} # Occupied by French troops
1807.7.9 = {
	owner = DNZ
	controller = DNZ
}
1814.1.2 = {
	owner = PRU
	controller = PRU
}
