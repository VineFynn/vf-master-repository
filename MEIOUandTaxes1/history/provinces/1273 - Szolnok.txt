# 1273 - Szolnok

owner = HUN
controller = HUN
add_core = HUN

capital = "Gyula"
trade_goods = livestock
culture = hungarian
religion = catholic

hre = no

base_tax = 7
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 8
	base_production = 0
	base_manpower = 0
}
1526.8.30 = {
	owner = TRA
	controller = TRA
	add_permanent_claim = HAB
	add_core = HUN
	add_core = TRA
}
1530.1.1 = {
	controller = HAB
}
1530.1.1 = {
	add_claim = TUR
}
1566.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
} # The foreign mercenaries betray the castle
1695.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
} # Last recapture from the Ottomans.
1703.8.1 = {
	controller = TRA
} # The town supports Francis II R�k�czi in his rebellion against the Habsburgs
1711.5.1 = {
	controller = HAB
} # Ceded to Austria The treaty of Szatmar, Austrian governors replaced the prince of Transylvania
1721.1.1 = {
	early_modern_university = yes
}
