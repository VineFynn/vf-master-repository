# 2531 - Napule

owner = KNP
controller = KNP
add_core = KNP

capital = "Napule"
trade_goods = olive #leather
culture = neapolitan
religion = catholic

hre = no

base_tax = 20
base_production = 6
base_manpower = 3

is_city = yes
local_fortification_1 = yes
marketplace = yes
urban_infrastructure_1 = yes
temple = yes
harbour_infrastructure_2 = yes
military_harbour_1 = yes
workshop = yes
road_network = yes
fort_14th = yes
small_university = yes #University of Naples Federico II

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = {
		name = "napoli_natural_harbour"
		duration = -1
	}
}
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_leather
		duration = -1
	}
}
1356.1.1 = {
	add_core = ANJ
}
1442.1.1 = {
	controller = ARA
	add_core = ARA
}
1495.2.22 = {
	controller = FRA
} # Charles VIII takes Napoli
1495.7.7 = {
	controller = KNP
} # Charles VIII leaves Italy
1502.1.1 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	fort_14th = yes
} # France and Aragon partitions Naples
1503.6.1 = {
	owner = ARA
	controller = ARA
	add_core = ARA
	remove_core = FRA
	remove_core = ANJ
} # France forced to withdraw
1504.1.31 = {
	remove_core = FRA
} # The Treaty of Lyon in 1504
1516.1.23 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = ARA
	road_network = no
	paved_road_network = yes
} # Unification of Spain
1520.5.5 = {
	art_corporation = yes # Naples School
	base_tax = 13
	base_production = 21
	base_manpower = 3
}
1522.3.20 = {
	naval_arsenal = yes
}
1530.1.1 = {
	owner = KNP
	controller = KNP
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
}
1714.3.7 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = SPA
}
1734.6.2 = {
	owner = NAP
	controller = NAP
	add_core = NAP
	remove_core = HAB
}
#1806.2.9 = {
#	controller = FRA
#} # French troops invade
#1815.5.3 = {
#	owner = KNP
#	controller = KNP
#	remove_core = NAP
#}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
