# No previous file for Winnipeg

capital = "Winnipeg"
trade_goods = unknown
culture = cree
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 2
native_hostileness = 5

450.1.1 = {
	set_province_flag = tribals_control_province
}
1710.1.1 = {
	owner = ASI
	controller = ASI
	add_core = ASI
	is_city = yes
	trade_goods = fur
	culture = nakota
} # Horses cause waves of migration on the Great Plains
1732.1.1 = { } # Pierre Gaultier de Varennes
1734.1.1 = {
	owner = FRA
	controller = FRA
	culture = francien
	religion = catholic
	capital = "Fort Maurepas"
	citysize = 100
}
1763.2.10 = {
	owner = GBR
	controller = GBR
	citysize = 450
	culture = english
	religion = protestant
	trade_goods = fur
} # Treaty of Paris
1788.2.10 = {
	add_core = GBR
}
