# 4007 - Milh�sa

owner = MLH
controller = MLH

capital = "Mulhausen"
trade_goods = wine
culture = rhine_alemanisch
religion = catholic

hre = yes

base_tax = 7
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
workshop = yes
local_fortification_1 = yes
marketplace = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = pocket_province
}
1354.1.1 = {
	owner = DCP
	controller = DCP
	add_core = DCP
}
1356.1.1 = {
	add_core = MLH
}
1500.1.1 = {
	road_network = yes
}
1515.1.1 = {
	owner = MLH
	controller = MLH
	remove_core = DCP
}
1520.5.5 = {
	base_tax = 8
	base_production = 1
	base_manpower = 0
}
1798.1.28 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Treaty of Mulhouse, after citizens vote to become part of France
1806.7.12 = {
	hre = no
}
