# 2856 - Bari

owner = KNP
controller = KNP
add_core = KNP

capital = "Bari"
trade_goods = olive
culture = neapolitan
religion = catholic

hre = no

base_tax = 11
base_production = 7
base_manpower = 1

is_city = yes
local_fortification_1 = yes
temple = yes
military_harbour_1 = yes
urban_infrastructure_2 = yes
marketplace = yes
harbour_infrastructure_2 = yes
workshop = yes
road_network = yes
warehouse = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1250.1.1 = {
	add_permanent_province_modifier = {
		name = "principality_of_taranto"
		duration = -1
	}
}
1356.1.1 = {
	add_core = ANJ
}
1442.1.1 = {
	add_core = ARA
}
1495.2.22 = {
	controller = FRA
} # Charles VIII invades Naples
1495.7.7 = {
	controller = KNP
} # Charles VIII leaves Italy
1502.1.1 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # France and Aragon partitions Naples
1503.6.1 = {
	owner = ARA
	controller = ARA
	add_core = ARA
	remove_core = FRA
	remove_core = ANJ
	fort_14th = yes
} # France forced to withdraw
1504.1.31 = {
	remove_core = FRA
} # Treaty of Lyon
1516.1.23 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = ARA
	road_network = no
	paved_road_network = yes
} # Unification of Spain
1520.5.5 = {
	base_tax = 13
	base_production = 10
	base_manpower = 1
}
1530.1.1 = {
	owner = KNP
	controller = KNP
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
}
1714.3.7 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = SPA
}
1734.6.2 = {
	owner = NAP
	controller = NAP
	add_core = NAP
	remove_core = HAB
}
#1815.5.3 = {
#	owner = KNP
#	controller = KNP
#	remove_core = NAP
#}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
