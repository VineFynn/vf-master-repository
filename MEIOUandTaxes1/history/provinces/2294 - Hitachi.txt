# 2294 - Hitachi
# GG/LS - Japanese Civil War

owner = STK
controller = STK
add_core = STK

capital = "Mito"
trade_goods = iron
culture = kanto
religion = mahayana #shinbutsu

hre = no

base_tax = 20
base_production = 1
base_manpower = 1

is_city = yes
town_hall = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = chinese

1490.1.1 = {
	owner = HJO
	controller = HJO
}
1501.1.1 = {
	base_tax = 33
	base_production = 2
	base_manpower = 3
}
1542.1.1 = {
	discovered_by = POR
}
1590.8.4 = {
	owner = ODA
	controller = ODA
} # Toyotomi Hideyoshi takes Odawara Castle, Hojo Ujimasa commits seppuku
1603.1.1 = {
	capital = "Mito"
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
