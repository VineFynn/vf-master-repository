# 3368 - Barbastro

owner = ARA
controller = ARA
add_core = ARA

capital = "Barbastro"
trade_goods = wool
culture = aragonese
religion = catholic

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
#road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1705.6.29 = {
	controller = HAB
} # Aragon joins the Austrian side in the War of Spanish Succession
1707.5.26 = {
	controller = SPA
} # Aragon surrenders
1713.7.13 = {
	remove_core = ARA
}
1813.8.31 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
