#374 - Leinster

owner = ENG
controller = ENG
add_core = ENG

capital = "Loch Garman"
trade_goods = wheat
culture = irish
religion = catholic

hre = no

base_tax = 1
base_production = 1
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
town_hall = yes
marketplace = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

700.1.1 = {
	add_permanent_province_modifier = {
		name = clan_land
		duration = -1
	}
}
1536.1.1 = {
	owner = ENG
	controller = ENG
	add_core = ENG
} # Succession of the Protestant James, 1st Duke of Ormond
1642.1.1 = {
	controller = REB
} # Estimated
1642.6.7 = {
	owner = IRE
	controller = IRE
} # Confederation of Kilkenny
1650.3.28 = {
	controller = ENG
} # Capture of Kilkenny
1652.4.1 = {
	owner = ENG
} # End of the Irish Confederates
1655.1.1 = {
	fort_14th = yes
}
1689.3.12 = {
	controller = REB
} # James II Lands in Ireland
1690.8.1 = {
	controller = ENG
} # Estimated
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
