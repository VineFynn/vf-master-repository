# 1547 - Touat

owner = TUA
controller = TUA
add_core = TUA

capital = "Adrar"
trade_goods = palm_date
culture = tuareg
religion = sunni

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 82 }
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	is_city = yes
}
1603.1.1 = {
	unrest = 5
} # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = {
	unrest = 0
}
1631.1.1 = {
	owner = TFL
	controller = TFL
}
1668.8.2 = {
	owner = MOR
	controller = MOR
}
