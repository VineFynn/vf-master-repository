# 112 - Venexia

owner = VEN
controller = VEN
add_core = VEN

capital = "Venexia"
trade_goods = fish #naval supplies and glassware after fall of Constantinople
culture = venetian
religion = catholic

hre = no

base_tax = 1
base_production = 11
base_manpower = 1

is_city = yes
urban_infrastructure_2 = yes
workshop = yes
merchant_guild = yes
harbour_infrastructure_3 = yes
warehouse = yes
military_harbour_2 = yes #Venice Arsenal
temple = yes
road_network = yes
fort_14th = yes

extra_cost = 30

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

450.1.1 = {
	add_permanent_province_modifier = {
		name = laguna_veneta
		duration = -1
	}
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = awesome_natural_place
}
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_naval_supplies
		duration = -1
	}
}
1400.1.1 = {
	art_corporation = yes # Venetian School
}
1470.1.1 = {
	small_university = yes
}
1520.5.5 = {
	base_tax = 1
	base_production = 17
	base_manpower = 2
}
1522.3.20 = {
	military_harbour_2 = no
	military_harbour_3 = yes
	art_corporation = no
	fine_arts_academy = yes #Biblioteca Marciana
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
	harbour_infrastructure_3 = no
	harbour_infrastructure_4 = yes
	manufactory = yes
	trade_depot = yes
}
1637.1.1 = {
	fine_arts_academy = no
	opera = yes # Teatro San Cassiano
}
1797.10.17 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} # Treaty of Campo Formio
1805.12.26 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = HAB
} # Treaty of Pressburg
1814.4.11 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1866.1.1 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
