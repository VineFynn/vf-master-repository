# 3346 - Awa (kanto)

owner = USG
controller = USG
add_core = USG

capital = "Hauzau"
trade_goods = fish
culture = kanto
religion = mahayana

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes

discovered_by = chinese

1356.1.1 = {
	add_core = YUK
	# Home province of Satomi ADD TAG
} # Held by Nanbu until it was lost during conflict
1363.1.1 = {
	controller = USG
	owner = USG
} # Taken back by Uyesugi
1369.1.1 = {
	controller = YUK
	owner = YUK
} # Held by yuki
1388.1.1 = {
	controller = USG
	owner = USG
}
1395.1.1 = {
	owner = YUK
	controller = YUK
} # Held by yuki
1397.1.1 = {
	controller = USG
	owner = USG
} #Held by Kido (vassal of the kamakura kubo(?))
1423.1.1 = {
	controller = USG
	owner = USG
} # Last recorded shugo
1490.1.1 = {
	owner = HJO
	controller = HJO
}
1501.1.1 = {
	base_tax = 9
}
1542.1.1 = {
	discovered_by = POR
}
1590.8.4 = {
	owner = ODA
	controller = ODA
} # Toyotomi Hideyoshi takes Odawara Castle, Hojo Ujimasa commits seppuku
1603.1.1 = {
	capital = "Mito"
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
