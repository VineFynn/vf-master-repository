#241 - Glamorgan

owner = ENG
controller = ENG
add_core = ENG

capital = "Cardiff"
trade_goods = iron
culture = welsh
religion = catholic

hre = no

base_tax = 10
base_production = 0
base_manpower = 1

is_city = yes
local_fortification_1 = yes
fort_14th = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

1284.3.3 = {
	add_permanent_province_modifier = {
		name = "marches_of_wales"
		duration = -1
	}
} # Statute of Rhuddlan
1356.1.1 = {
	add_core = WLS
}
1453.1.1 = {
	unrest = 5
} # Start of the War of the Roses
1461.6.1 = {
	unrest = 2
} # Coronation of Edward IV
1467.1.1 = {
	unrest = 5
} # Rivalry between Edward IV & Warwick
1471.1.1 = {
	unrest = 8
} # Unpopularity of Warwick & War with Burgundy
1471.3.1 = {
	controller = REB
}
1471.5.4 = {
	unrest = 2
	controller = ENG
} # Murder of Henry VI & Restoration of Edward IV
1472.1.1 = {
	remove_province_modifier = "marches_of_wales"
	add_permanent_province_modifier = {
		name = "council_of_wales"
		duration = -1
	}
} # Council of Wales and the Marches, exact date uncertain
1483.6.26 = {
	unrest = 8
} # Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = {
	unrest = 0
} # Battle of Bosworth Field & the End of the War of the Roses
1501.1.1 = {
	base_tax = 16
}
1542.3.25 = {
	remove_province_modifier = "council_of_wales"
	add_permanent_province_modifier = {
		name = "laws_in_wales"
		duration = -1
	}
} # The Laws in Wales Act, exact date uncertain
1600.1.1 = {
	fort_14th = yes
} # Estimated
1645.10.1 = {
	controller = REB
} # Estimated
1646.5.5 = {
	controller = ENG
} # End of First English Civil War
1648.3.15 = {
	controller = REB
} # Start of Second English Civil War
1648.7.11 = {
	controller = ENG
}
1689.7.25 = {
	remove_province_modifier = "laws_in_wales"
} # Council abolished following the Glorious Revolution of 1688
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1750.1.1 = {
	religion = reformed
}
