# No previous file for Szabolcs

owner = HUN
controller = HUN
add_core = HUN

capital = "Debrecen"
trade_goods = wheat
culture = hungarian
religion = catholic

hre = no

base_tax = 8
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
temple = yes
local_fortification_1 = yes
marketplace = yes
workshop = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 9
	base_production = 1
	base_manpower = 0
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = TRA
}
1530.1.1 = {
	add_claim = TUR
}
1541.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1685.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
