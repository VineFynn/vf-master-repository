# 2924 - Gobir

owner = GOB
controller = GOB
add_core = GOB

capital = "Alkalawa"
trade_goods = cotton
culture = haussa
religion = sunni

hre = no

base_tax = 13
base_production = 3
base_manpower = 1

is_city = yes
urban_infrastructure_2 = yes
marketplace = yes
workshop = yes
fort_14th = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 15 }
	set_variable = { which = settlement_score_progress_preset	value = 46 }
}
1520.1.1 = {
	base_tax = 21
}