name="MEIOU and Taxes North America Natives Module v3.1"
path="mod/MEIOUandTaxesNatives"
dependencies={
	"MEIOU and Taxes 1.26"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxes6.jpg"
supported_version="1.18.*.*"
