# No previous file for Kawaiisu

culture = hopi
religion = totemism
capital = "Kawaiisu"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1731.1.1   = { owner = PIM
		controller = PIM
		add_core = PIM
		trade_goods = wool
		is_city = yes }
