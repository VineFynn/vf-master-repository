# No previous file for Jicarilla

owner = PUE
controller = PUE
add_core = PUE
is_city = yes
culture = navajo
religion = totemism
capital = "Jicarilla"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 10
native_ferocity = 4
native_hostileness = 8

1540.1.1   = {  } # Francisco Vasquez de Coronado
1598.1.1   = {
	owner = SPA
	controller = SPA
	citysize = 350
	religion = catholic
	culture = castillian
	trade_goods = wool 
} # First Spanish colonists, early El Paso was on this side of the river.
1623.1.1   = { add_core = SPA is_city = yes }
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
