# No previous file for Tsoyaha

owner = CHI
controller = CHI
add_core = CHI
is_city = yes
culture = chickasaw
religion = totemism
capital = "Tsoyaha"
trade_goods = cotton
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6

1786.1.10= {
	owner = USA
	controller = USA
	add_core = USA
	culture = american
	religion = protestant
} #Treaty of Hopewell (with the Chickasaw), come under US authority
