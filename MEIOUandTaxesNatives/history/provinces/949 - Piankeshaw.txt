# No previous file for Piankeshaw

culture = illini
religion = totemism
capital = "Piankeshaw"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 20
native_ferocity = 2
native_hostileness = 5


1650.1.1  = {	owner = ILN
		controller = ILN
		add_core = ILN
		is_city = yes }
1659.1.1  = {  } # M�dard Chouart Des Groseilliers
1724.1.1  = {	owner = FRA
		controller = FRA
		citysize = 250
		culture = francien
		religion = catholic
	    }
1749.1.1  = { add_core = FRA }
1762.1.1  = {	owner = SPA
		controller = SPA
		remove_core = FRA
	    } # Treaty of Fontainebleau, secretely ceded to Spain
1787.1.1  = { add_core = SPA }
1800.10.1 = {	owner = FRA
		controller = FRA
		add_core = FRA
	    	remove_core = SPA
	    } # Treaty of San Ildefonso
1803.4.3  = {	owner = USA
		controller = USA
		add_core = USA
		remove_core = FRA
	    } # The Louisiana purchase
