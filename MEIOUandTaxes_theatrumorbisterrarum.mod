name="Theatrum Orbis Terrarum for MEIOU & Taxes 2.02"
path="mod/MEIOUandTaxes_theatrumorbisterrarum"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"Graphics"
	"Map"
	"Fixes"
	"MEIOU and Taxes"
	"Theatrum Orbis Terrarum"
}
picture="image.png"
supported_version="1.24.*.*"
