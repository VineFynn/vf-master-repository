cb_jp_imperial_restoration = {
	ai_peace_desire = -90
	
	prerequisites = {

		OR = {	#Emperor
			government = early_medieval_japanese_gov
			government = late_medieval_japanese_gov
			government = early_modern_japanese_gov
		}
		government_rank = 6
		is_free_or_tributary_trigger = yes
		OR = {
			ai = no
			AND = {
				ai = yes
				NOT = { has_opinion = { who = FROM value = -100 } }
			}
		}
		FROM = {
			OR = { #Shogunate
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			government_rank = 5
			NOT = { government_rank = 6 }
			NOT = { is_subject_of = ROOT }
		}
	}

	war_goal = imperial_restoration_wargoal
}

cb_unify_courts = {
	ai_peace_desire = -90
	
	prerequisites = {

		OR = { #Shogunate
			government = early_medieval_japanese_gov
			government = late_medieval_japanese_gov
			government = early_modern_japanese_gov
		}
		government_rank = 5
		NOT = { government_rank = 6 }
		FROM = {
			OR = { #Emperor
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			government_rank = 6
		}
	}

	war_goal = imperial_restoration_wargoal
}