nanboku_cho = { # GG - requires TM reactivity
	potential = {
		japan_region = { owned_by = ROOT }
		has_global_flag = imperial_court_divided
	}

	trigger = {
		OR = {
			AND = {
				exists = AKG
				war_with = AKG
			}
			AND = {
				exists = JAP
				war_with = JAP
			}
		}		
	}
	#global_regiment_cost = -5.00
	global_regiment_recruit_speed = -5.00
	stability_cost_modifier = 0.5
#	discipline = 0.10
	land_forcelimit = 2
	global_manpower = 5
	global_manpower_modifier = 1
	manpower_recovery_speed = 0.5
	mercenary_cost = -0.5
	global_unrest = 5
	legitimacy = -2
	#naval_forcelimit = -30
	defensiveness = 0.35
	liberty_desire = -100
	vassal_income = -0.3
}
