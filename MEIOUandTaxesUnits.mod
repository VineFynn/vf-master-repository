name="MEIOU and Taxes Complete Units Module"
path="mod/MEIOUandTaxesUnits"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
	"beta test"
}
picture="MEIOUandTaxesUS.jpg"
supported_version="1.24.*.*"
